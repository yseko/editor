﻿using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class MasterTableWindow : EditorWindow, IMasterTableDataListener
{
    public static void Open(TextAsset asset)
    {
        var window = GetWindow<MasterTableWindow>("MasterTable");
        window.Init(asset);
    }

    TextAsset _asset;
    private MasterTableData tableData;
    private MasterTableView tableView;
    private SearchField searchField;

    void Init(TextAsset asset)
    {
        _asset = asset;

        tableData = new MasterTableData(_asset, this);
        tableData.Load();

        var state = new TreeViewState();
        var header = new MasterTableHeader(null, tableData.Columns.ToArray());
        tableView = new MasterTableView(state, header, tableData);
        searchField = new SearchField();
        searchField.downOrUpArrowKeyPressed += tableView.SetFocusAndEnsureSelectedItem;
    }

    private void OnEnable()
    {
        if (_asset != null)
            Init(_asset);
    }

    private void OnGUI()
    {
        using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
        {
            if (GUILayout.Button("Reload", EditorStyles.toolbarButton, GUILayout.Width(100)))
            {
                tableView.Reload();
            }
            if (GUILayout.Button("Save", EditorStyles.toolbarButton, GUILayout.Width(100)))
            {
                tableView.Save();
            }

            using (var checkScope = new EditorGUI.ChangeCheckScope())
            {
                var searchString = searchField.OnToolbarGUI(tableView.searchString);
                if (checkScope.changed)
                    tableView.searchString = searchString;
            }
        }

        tableView?.OnGUI(new Rect(0, EditorGUIUtility.singleLineHeight + 1, position.width,
            position.height - EditorGUIUtility.singleLineHeight - 1));
    }

    public void OnChangedColumnType(MasterActualColumn column)
    {
        tableData.Update(column);
        tableView.Reload();
    }
}
