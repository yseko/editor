﻿using System;
using System.Linq;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class MasterTableView : TreeView
{
    MasterTableData data;

    public MasterTableView(TreeViewState state, MultiColumnHeader multiColumnHeader, MasterTableData data) : base(state, multiColumnHeader)
    {
        this.data = data;

        rowHeight = 20;
        showAlternatingRowBackgrounds = true;
        multiColumnHeader.sortingChanged += SortItems;
        multiColumnHeader.ResizeToFit();
        Reload();
    }

    public void Save()
    {
        var rows = GetRows();
        rows.ForEach(row =>
        {
            Debug.LogFormat("id = {0}", row.id);
        });
    }

    public void Reload(MasterActualColumn column)
    {
        Reload();
    }

    protected override TreeViewItem BuildRoot()
    {
        var rootItem = new TreeViewItem
        {
            depth = -1
        };
        data.Items.ForEach(item => rootItem.AddChild(item));
        return rootItem;
    }

    protected override void RowGUI(RowGUIArgs args)
    {
        var item = (MasterTableRow)args.item;
        item.Row = args.row;

        for (var visibleColumnIndex = 0; visibleColumnIndex < args.GetNumVisibleColumns(); visibleColumnIndex++)
        {
            var rect = args.GetCellRect(visibleColumnIndex);
            var columnIndex = args.GetColumn(visibleColumnIndex);
            item.Draw(columnIndex, rect, args.selected);
        }
    }

    protected override bool DoesItemMatchSearch(TreeViewItem item, string search)
    {
        var rowItem = (MasterTableRow)item;
        return rowItem.Cells
            .Where(r => r.ColumnId > 0) // 行数は検索対象に含めない
            .Any(r => r.Value.IndexOf(search, StringComparison.OrdinalIgnoreCase) >= 0);
    }

    void SortItems(MultiColumnHeader multiColumnHeader)
    {
        var columnIndex = multiColumnHeader.sortedColumnIndex;
        var column = (MasterActualColumn)data.Columns.Find(x => x.Id == columnIndex);
        var ascending = multiColumnHeader.IsSortedAscending(columnIndex);
        var items = rootItem.children.Cast<MasterTableRow>().ToList();
        items.Sort((a, b) => MasterTableRow.Compare(column, a, b));
        if (!ascending)
            items.Reverse();
        rootItem.children = items.Cast<TreeViewItem>().ToList();
        BuildRows(rootItem);
    }
}
