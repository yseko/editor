﻿using UnityEditor.IMGUI.Controls;
using UnityEngine;
using Editor.TableView;

public class MasterTableHeader : TableViewHeader
{
    public MasterTableHeader(MultiColumnHeaderState state, MultiColumnHeaderState.Column[] columns) : base(state, columns)
    {
        height = 35f;
        this.state = new MultiColumnHeaderState(columns);
    }

    protected override void ColumnHeaderGUI(MultiColumnHeaderState.Column column, Rect headerRect, int columnIndex)
    {
        if (column is MasterColumn)
            ((MasterColumn)column).OnGUI(this, headerRect, columnIndex);
    }
}
