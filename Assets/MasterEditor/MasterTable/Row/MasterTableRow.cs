﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class MasterTableRow : TreeViewItem
{
    public List<MasterRowCell> Cells;
    public MasterRowCell this[int columnId] { get { return Cells[columnId]; } }
    public int Row { get { return 0; } set { Cells[0].Value = value.ToString(); } }

    public MasterTableRow(int id, string[] values, MasterColumn[] columns) : base(id)
    {
        Cells = new List<MasterRowCell>();
        for (int i = 0; i < columns.Length; i++)
        {
            Cells.Add(CreateValue(columns[i], values[i]));
        }
    }

    public void Update(MasterColumn column)
    {
        var v = Cells.First(x => x.ColumnId == column.Id);
        var index = Cells.IndexOf(v);
        Cells[index] = CreateValue(column, v.Value);
    }

    public void Draw(int columnIndex, Rect rect, bool selected)
    {
        Cells[columnIndex].Draw(rect, selected);
    }

    MasterRowCell CreateValue(MasterColumn column, string value)
    {
        // 行数
        if (column is MasterLineColumn)
            return new MasterRowLineCell(column.Id);

        switch (column.DrawType)
        {
            case MasterRowCell.DrawType.String: return new MastarRowStringCell(column.Id, value);
            case MasterRowCell.DrawType.Bool: return new MasterRowBoolCell(column.Id, value);
            case MasterRowCell.DrawType.Integer: return new MasterRowIntCell(column.Id, value);
            case MasterRowCell.DrawType.Long: return new MasterRowLongCell(column.Id, value);
            default: return new MasterRowLabelCell(column.Id, value);
        }
    }

    public static int Compare(MasterActualColumn column, MasterTableRow a, MasterTableRow b)
    {
        switch (column.ValueType)
        {
            case MasterRowValueType.String:
                return string.Compare(a[column.Id].Value, b[column.Id].Value);
            case MasterRowValueType.Bool:
                {
                    var x = (MasterRowBoolCell)a[column.Id];
                    var y = (MasterRowBoolCell)b[column.Id];
                    if (x.SelectIndex < y.SelectIndex) return -1;
                    else if (x.SelectIndex > y.SelectIndex) return 1;
                }
                break;
            case MasterRowValueType.Integer:
            case MasterRowValueType.Long:
                {
                    var x = long.Parse(b[column.Id].Value);
                    var y = long.Parse(a[column.Id].Value);
                    if (x > y) return -1;
                    else if (x < y) return 1;
                }
                break;
        }
        return 0;
    }
}
