﻿using UnityEditor;
using UnityEngine;

public abstract class MasterRowCell : IRowCell
{
    public string Value { get; set; }
    public int ColumnId { get; set; }

    public enum DrawType
    {
        Label,
        Bool,
        Integer,
        Long,
        String,
    }

    public abstract void Draw(Rect rect, bool selected);

    public MasterRowCell(int columnId, string value)
    {
        ColumnId = columnId;
        Value = value;
    }
}

/// <summary>
/// 行数表示用
/// </summary>
public class MasterRowLineCell : MasterRowLabelCell
{
    public int Line { get { return int.Parse(Value); } set { Value = value.ToString(); } }

    public MasterRowLineCell(int columnId) : base(columnId, null)
    {
    }
}

/// <summary>
/// ラベル表示
/// </summary>
public class MasterRowLabelCell : MasterRowCell
{
    public MasterRowLabelCell(int columnId, string value) : base(columnId, value)
    {
    }

    public override void Draw(Rect rect, bool selected)
    {
        var labelStyle = selected ? EditorStyles.whiteLabel : EditorStyles.label;
        labelStyle.alignment = TextAnchor.MiddleLeft;
        EditorGUI.LabelField(rect, Value, labelStyle);
    }
}

/// <summary>
/// string用
/// </summary>
public class MastarRowStringCell : MasterRowCell
{
    public MastarRowStringCell(int columnId, string value) : base(columnId, value)
    {
    }

    public override void Draw(Rect rect, bool selected)
    {
        var labelStyle = selected ? EditorStyles.whiteLabel : EditorStyles.label;
        labelStyle.alignment = TextAnchor.MiddleLeft;
        Value = EditorGUI.TextField(rect, Value, labelStyle);
    }
}

/// <summary>
/// bool用
/// </summary>
public class MasterRowBoolCell : MasterRowCell
{
    public int SelectIndex { get; private set; }

    public MasterRowBoolCell(int columnId, string value) : base(columnId, value)
    {
        SelectIndex = value == "True" ? 1 : 0;
    }

    public override void Draw(Rect rect, bool selected)
    {
        SelectIndex = EditorGUI.Popup(rect, SelectIndex, new string[] { "False", "True" });
    }
}

/// <summary>
/// int用
/// </summary>
public class MasterRowIntCell : MasterRowCell
{
    int _value;

    public MasterRowIntCell(int columnId, string value) : base(columnId, value)
    {
        try
        {
            this._value = int.Parse(value);
        }
        catch
        {
        }
    }

    public override void Draw(Rect rect, bool selected)
    {
        _value = EditorGUI.IntField(rect, _value);
    }
}

/// <summary>
/// long用
/// </summary>
public class MasterRowLongCell : MasterRowCell
{
    long _value;

    public MasterRowLongCell(int columnId, string value) : base(columnId, value)
    {
        try
        {
            this._value = int.Parse(value);
        }
        catch
        {
        }
    }

    public override void Draw(Rect rect, bool selected)
    {
        _value = EditorGUI.LongField(rect, _value);
    }
}
