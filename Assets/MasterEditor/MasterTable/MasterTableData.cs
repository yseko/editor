﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class MasterTableData
{
    public List<MasterColumn> Columns;
    public List<MasterTableRow> Items;

    private TextAsset source;
    IMasterTableDataListener listener;

    public MasterTableData(TextAsset source, IMasterTableDataListener listener)
    {
        this.source = source;
        this.listener = listener;
    }

    public void Load()
    {
        var reader = new StringReader(source.text);
        var columnFactory = new MasterColumnFactory();
        Columns = columnFactory.ReadColumns(reader);
        Columns.OfType<MasterActualColumn>().ForEach(x => x.OnChangedType += listener.OnChangedColumnType);
        Items = ExtractRows(reader);
    }

    public void Update(MasterActualColumn column)
    {
        column.OnChangedType += listener.OnChangedColumnType;
        var c = Columns.First(x => x.Id == column.Id);
        var index = Columns.IndexOf(c);
        Columns[index] = column;
        Items.ForEach(x => x.Update(column));
    }

    List<MasterTableRow> ExtractRows(StringReader reader)
    {
        // 1行目はカラム名なので無視
        reader.ReadLine();

        var rows = new List<MasterTableRow>();
        int line = 0;
        while (reader.Peek() != -1)
        {
            var values = new List<string>();
            values.Add(line.ToString());
            values.AddRange(reader.ReadLine().Split(','));
            rows.Add(new MasterTableRow(line, values.ToArray(), Columns.ToArray()));
            line++;
        }
        return rows;
    }
}

public interface IMasterTableDataListener
{
    void OnChangedColumnType(MasterActualColumn column);
}