﻿using UnityEditor.IMGUI.Controls;
using UnityEngine;

public abstract class MasterColumn : MultiColumnHeaderState.Column
{
    public int Id { get; }
    public MasterRowCell.DrawType DrawType { get; protected set; }
    public abstract void OnGUI(MasterTableHeader header, Rect rect, int index);

    protected MasterColumn(int id)
    {
        Id = id;
    }
}