﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class MasterActualColumn : MasterColumn
{
    public string Name;
    public UnityAction<MasterActualColumn> OnChangedType;
    public MasterRowValueType ValueType;

    public MasterActualColumn(int id, string name) : this(id)
    {
        Name = name;
    }

    public MasterActualColumn(int id) : base(id)
    {
        width = 100;
        headerContent = new GUIContent("");
    }

    public override void OnGUI(MasterTableHeader header, Rect rect, int index)
    {
        float textHeight = EditorGUIUtility.singleLineHeight;
        float y = rect.y;

        float sortButtonWidth = 0f;
        if (canSort)
        {
            sortButtonWidth = 20f;
            header.SortButton(this, new Rect(rect.x + rect.width - sortButtonWidth, y, sortButtonWidth, textHeight), index);
        }

        Name = GUI.TextField(new Rect(rect.x, y, rect.width - sortButtonWidth, textHeight), Name);
        y += textHeight + 3f;

        EditorGUI.BeginChangeCheck();
        ValueType = (MasterRowValueType)EditorGUI.EnumPopup(new Rect(rect.x, y, rect.width, header.height - y), ValueType);
        if (EditorGUI.EndChangeCheck())
        {
            switch (ValueType)
            {
                case MasterRowValueType.String: DrawType = MasterRowCell.DrawType.String; break;
                case MasterRowValueType.Bool: DrawType = MasterRowCell.DrawType.Bool; break;
                case MasterRowValueType.Integer: DrawType = MasterRowCell.DrawType.Integer; break;
                case MasterRowValueType.Long: DrawType = MasterRowCell.DrawType.Long; break;
            }
            OnChangedType.Invoke(this);
        }
    }
}

public enum MasterRowValueType
{
    String,
    Bool,
    Integer,
    Long,
}
