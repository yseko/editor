﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

public class MasterColumnFactory
{
    public int columnId;

    public List<MasterColumn> ReadColumns(StringReader reader)
    {
        var list = new List<MasterColumn>();
        list.Add(new MasterLineColumn(columnId++));
        list.AddRange(reader.ReadLine().Split(',').Select(x => Create(x)));
        return list;
    }

    public MasterActualColumn Create(string name)
    {
        return new MasterActualColumn(columnId++, name);
    }
}
