﻿using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class MasterLineColumn : MasterColumn
{
    public MasterLineColumn(int id) : base(id)
    {
        width = minWidth = maxWidth = 32;
        headerContent = new GUIContent("line");
        DrawType = MasterRowCell.DrawType.Label;
        canSort = false;
    }

    public override void OnGUI(MasterTableHeader header, Rect rect, int index)
    {
        GUIStyle style = GetStyle(this.headerTextAlignment);
        float labelHeight = EditorGUIUtility.singleLineHeight;
        Rect labelRect = new Rect(rect.x, rect.yMax - labelHeight - 3f, rect.width, labelHeight);
        GUI.Label(labelRect, this.headerContent, style);
    }

    GUIStyle GetStyle(TextAlignment alignment)
    {
        switch (alignment)
        {
            case TextAlignment.Left: return MultiColumnHeader.DefaultStyles.columnHeader;
            case TextAlignment.Center: return MultiColumnHeader.DefaultStyles.columnHeaderCenterAligned;
            case TextAlignment.Right: return MultiColumnHeader.DefaultStyles.columnHeaderRightAligned;
            default: return MultiColumnHeader.DefaultStyles.columnHeader;
        }
    }
}
