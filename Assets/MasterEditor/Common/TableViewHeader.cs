﻿using UnityEditor.IMGUI.Controls;
using UnityEngine;
using UnityEditor;

namespace Editor.TableView
{
    public class TableViewHeader : MultiColumnHeader
    {   
        public TableViewHeader(MultiColumnHeaderState state, MultiColumnHeaderState.Column[] columns) : base(state)
        {
            height = 22f;
            this.state = new MultiColumnHeaderState(columns);
        }

        public void SortButton(MultiColumnHeaderState.Column column, Rect headerRect, int columnIndex)
        {
            var rect = new Rect(headerRect.x, headerRect.y + 2, headerRect.width - 4, headerRect.height);
            if (GUI.Button(rect, GUIContent.none, GUI.skin.box))
            {
                ColumnHeaderClicked(column, columnIndex);
            }

            if (columnIndex == state.sortedColumnIndex && Event.current.type == EventType.Repaint)
            {
                var labelRect = new Rect(rect.x + 2.5f, rect.y + 2, rect.width - 5, rect.height);

                Matrix4x4 normalMatrix = GUI.matrix;
                if (column.sortedAscending)
                    GUIUtility.RotateAroundPivot(180, labelRect.center - new Vector2(0, 2));

                GUI.Label(labelRect, "\u25BE");

                if (column.sortedAscending)
                    GUI.matrix = normalMatrix;
            }
        }
    }
}
