﻿using UnityEditor;
using UnityEngine;

public class RowLabelCell : IRowCell
{
    public int ColumnId { get; set; }
    public string Value { get; set; }

    public RowLabelCell(int columnId, string value)
    {
        ColumnId = columnId;
        Value = value;
    }

    public void Draw(Rect rect, bool selected)
    {
        var labelStyle = selected ? EditorStyles.whiteLabel : EditorStyles.label;
        labelStyle.alignment = TextAnchor.MiddleLeft;
        EditorGUI.LabelField(rect, Value, labelStyle);
    }
}
