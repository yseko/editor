﻿using UnityEngine;

public interface IRowCell
{
    int ColumnId { get; set; }
    string Value { get; set; }

    void Draw(Rect rect, bool selected);
}
