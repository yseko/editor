﻿using System.IO;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using UnityEditor;

public class MasterListRow : TreeViewItem
{
    public TextAsset Asset;
    public IRowCell[] Cells;
    public IRowCell this[MasterListColumn.Id index] { get { return Cells[(int)index]; } }

    public MasterListRow(int id, TextAsset asset) : base(id)
    {
        Asset = asset;

        var reader = new StringReader(asset.text);
        var lineNum = -1; // カラム名入ってるので-1から
        while (reader.Peek() != -1)
        {
            reader.ReadLine();
            lineNum++;
        }

        Cells = new []
        {
            new RowLabelCell((int)MasterListColumn.Id.Name, asset.name),
            new RowLabelCell((int)MasterListColumn.Id.Size, EditorUtility.FormatBytes(asset.bytes.LongLength)),
            new RowLabelCell((int)MasterListColumn.Id.DataCount, lineNum.ToString()),
        };
    }

    public void Draw(Rect rect, int columIndex, bool selected)
    {
        Cells[columIndex].Draw(rect, selected);
    }

    public static int Compare(int columnIndex, MasterListRow a, MasterListRow b)
    {
        var columnId = (MasterListColumn.Id)columnIndex;
        switch (columnId)
        {
            case MasterListColumn.Id.Name:
                return string.Compare(a[columnId].Value, b[columnId].Value);
            case MasterListColumn.Id.Size:
            case MasterListColumn.Id.DataCount:
                {
                    var x = long.Parse(b[columnId].Value);
                    var y = long.Parse(a[columnId].Value);
                    if (x > y) return -1;
                    else if (x < y) return 1;
                }
                break;
        }
        return 0;
    }
}
