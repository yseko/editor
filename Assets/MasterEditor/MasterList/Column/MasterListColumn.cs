﻿using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class MasterListColumn : MultiColumnHeaderState.Column
{
    public enum Id
    {
        Name,
        Size,
        DataCount,
    }

    public MasterListColumn(Id id)
    {
        width = 32;
        headerContent = new GUIContent(id.ToString());
    }

    public virtual void OnGUI(MasterListHeader header, Rect rect, int index)
    {
        float textHeight = EditorGUIUtility.singleLineHeight;
        float y = rect.y;

        float sortButtonWidth = 0f;
        if (canSort)
        {
            sortButtonWidth = 20f;
            header.SortButton(this, new Rect(rect.x + rect.width - sortButtonWidth, y, sortButtonWidth, textHeight), index);
        }

        GUIStyle style = GetStyle(this.headerTextAlignment);
        GUI.Label(new Rect(rect.x, y, rect.width - sortButtonWidth, textHeight), this.headerContent, style);
    }

    GUIStyle GetStyle(TextAlignment alignment)
    {
        switch (alignment)
        {
            case TextAlignment.Left: return MultiColumnHeader.DefaultStyles.columnHeader;
            case TextAlignment.Center: return MultiColumnHeader.DefaultStyles.columnHeaderCenterAligned;
            case TextAlignment.Right: return MultiColumnHeader.DefaultStyles.columnHeaderRightAligned;
            default: return MultiColumnHeader.DefaultStyles.columnHeader;
        }
    }
}
