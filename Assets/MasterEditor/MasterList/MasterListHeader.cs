﻿using UnityEditor.IMGUI.Controls;
using UnityEngine;
using Editor.TableView;

public class MasterListHeader : TableViewHeader
{
    public MasterListHeader(MultiColumnHeaderState state, MultiColumnHeaderState.Column[] columns) : base(state, columns)
    {
        height = 22f;
        this.state = new MultiColumnHeaderState(columns);
    }

    protected override void ColumnHeaderGUI(MultiColumnHeaderState.Column column, Rect headerRect, int columnIndex)
    {
        ((MasterListColumn)column).OnGUI(this, headerRect, columnIndex);
    }
}
