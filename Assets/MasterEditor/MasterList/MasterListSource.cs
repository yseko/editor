﻿using System.Linq;
using UnityEngine;

public class MasterListSource
{
    public MasterListColumn[] Columns;
    public MasterListRow[] Items;

    public void Load()
    {
        Columns = new []
        {
            new MasterListColumn(MasterListColumn.Id.Name),
            new MasterListColumn(MasterListColumn.Id.Size),
            new MasterListColumn(MasterListColumn.Id.DataCount),
        };
        var assets = Resources.LoadAll<TextAsset>("Masters");
        var id = 0;
        Items = assets.Select(x => new MasterListRow(id++, x)).ToArray();
    }
}
