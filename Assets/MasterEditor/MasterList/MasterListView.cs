﻿using System;
using System.Linq;
using UnityEditor.IMGUI.Controls;

public class MasterListView : TreeView
{
    MasterListSource source;

    public MasterListView(TreeViewState state, MultiColumnHeader header, MasterListSource source) : base(state, header)
    {
        this.source = source;

        rowHeight = 20;
        showAlternatingRowBackgrounds = true;
        multiColumnHeader.sortingChanged += SortItems;
        multiColumnHeader.ResizeToFit();
        Reload();
    }

    protected override TreeViewItem BuildRoot()
    {
        var rootItem = new TreeViewItem
        {
            depth = -1
        };
        source.Items.ForEach(item => rootItem.AddChild(item));
        return rootItem;
    }

    protected override void RowGUI(RowGUIArgs args)
    {
        var item = (MasterListRow)args.item;
        for (var visibleColumnIndex = 0; visibleColumnIndex < args.GetNumVisibleColumns(); visibleColumnIndex++)
        {
            var rect = args.GetCellRect(visibleColumnIndex);
            var columnIndex = args.GetColumn(visibleColumnIndex);
            item.Draw(rect, columnIndex, args.selected);
        }
    }

    /// <summary>
    /// ダブルクリック
    /// </summary>
    /// <param name="id">Identifier.</param>
    protected override void DoubleClickedItem(int id)
    {
        // マスターの内容を表示
        var row = source.Items.First(x => x.id == id);
        MasterTableWindow.Open(row.Asset);
    }

    /// <summary>
    /// 検索
    /// </summary>
    /// <returns><c>true</c>, if item match search was doesed, <c>false</c> otherwise.</returns>
    /// <param name="item">Item.</param>
    /// <param name="search">Search.</param>
    protected override bool DoesItemMatchSearch(TreeViewItem item, string search)
    {
        var rowItem = (MasterListRow)item;
        return rowItem[MasterListColumn.Id.Name].Value.IndexOf(search, StringComparison.OrdinalIgnoreCase) >= 0;
    }

    /// <summary>
    /// ソート
    /// </summary>
    /// <param name="multiColumnHeader">Multi column header.</param>
    void SortItems(MultiColumnHeader multiColumnHeader)
    {
        var columnIndex = multiColumnHeader.sortedColumnIndex;
        var ascending = multiColumnHeader.IsSortedAscending(columnIndex);
        var items = rootItem.children.Cast<MasterListRow>().ToList();
        items.Sort((a, b) => MasterListRow.Compare(columnIndex, a, b));
        if (!ascending)
            items.Reverse();
        rootItem.children = items.Cast<TreeViewItem>().ToList();
        BuildRows(rootItem);
    }
}
