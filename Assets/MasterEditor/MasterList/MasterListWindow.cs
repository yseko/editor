﻿using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class MasterListWindow : EditorWindow
{
    [MenuItem("Tools/MasterList")]
    static void Open()
    {
        GetWindow<MasterListWindow>("MasterList");
    }

    private MasterListSource dataSource;
    private MasterListView listView;
    private SearchField searchField;

    private void OnEnable()
    {
        dataSource = new MasterListSource();
        dataSource.Load();

        var state = new TreeViewState();
        var header = new MasterListHeader(null, dataSource.Columns.ToArray());
        listView = new MasterListView(state, header, dataSource);
        searchField = new SearchField();
        searchField.downOrUpArrowKeyPressed += listView.SetFocusAndEnsureSelectedItem;
    }

    private void OnGUI()
    {
        using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
        {
            if (GUILayout.Button("Reload", EditorStyles.toolbarButton, GUILayout.Width(100)))
            {
                listView.Reload();
            }

            using (var checkScope = new EditorGUI.ChangeCheckScope())
            {
                var searchString = searchField.OnToolbarGUI(listView.searchString);
                if (checkScope.changed)
                    listView.searchString = searchString;
            }
        }

        listView?.OnGUI(new Rect(0, EditorGUIUtility.singleLineHeight + 1, position.width,
            position.height - EditorGUIUtility.singleLineHeight - 1));
    }
}
