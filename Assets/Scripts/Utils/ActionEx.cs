﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace System
{
    public static class ActionEx
    {
        public static void SafeCall(this Action self)
        {
            if (self != null)
                self();
        }

        public static void SafeCall<T>(this Action<T> self, T arg)
        {
            if (self != null)
                self(arg);
        }

        public static void SafeCall<T1, T2>(this Action<T1, T2> self, T1 arg1, T2 arg2)
        {
            if (self != null)
                self(arg1, arg2);
        }

        public static void SafeCall<T1, T2, T3>(this Action<T1, T2, T3> self, T1 arg1, T2 arg2, T3 arg3)
        {
            if (self != null)
                self(arg1, arg2, arg3);
        }

        public static void SafeCall<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> self, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            if (self != null)
                self(arg1, arg2, arg3, arg4);
        }
    }
}
