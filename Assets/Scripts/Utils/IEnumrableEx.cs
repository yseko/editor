﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace System.Linq
{
    public static class IEnumerableEx
    {
        public static void ForEach<T>(this IEnumerable<T> self, Action<T> action)
        {
            foreach (var value in self)
                action(value);
        }
    }

    public static class IDictionaryEx
    {
        public static void ForEach<TKey, TValue>(this IDictionary<TKey, TValue> self, Action<TKey, TValue> action)
        {
            foreach(var key in self.Keys)
            {
                action(key, self[key]);
            }
        }
    }
}
